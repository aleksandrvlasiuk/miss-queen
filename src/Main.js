import React, { Component } from 'react';
import Head from './components/mainPage/head/Head';
import SideBar from './components/mainPage/head/SideBar/SideBar';
import About from './components/mainPage/about/About';
import Goals from './components/mainPage/goals/Goals';
import Services from './components/mainPage/services/Services';
import Parthners from './components/mainPage/parthners/Parthners';
import Footer from './components/mainPage/footer/Footer';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import ScrollableAnchor from 'react-scrollable-anchor';
import { configureAnchors } from 'react-scrollable-anchor';

configureAnchors({
	scrollDuration: 600
})

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			language: localStorage.getItem('language') || "Eng"
		}
	}
	updateData = (value) => {
		this.setState({ language: value });
	}
	render() {
		return (
			
			<div className="" id="outer-container">
				<div id="page-wrap">
					<SideBar main lang={this.state.language}/>
					<ScrollableAnchor id={'home'}>
					<Head main lang={this.state.language} updateData={this.updateData} />
					</ScrollableAnchor>
					<ScrollableAnchor id={'about'}>
						<About main lang={this.state.language}/>
					</ScrollableAnchor>
					<ScrollableAnchor id={'goals'}>
						<Goals main lang={this.state.language}/>
					</ScrollableAnchor>
					<ScrollableAnchor id={'services'}>
						<Services main lang={this.state.language}/>
					</ScrollableAnchor>	
					<ScrollableAnchor id={'parthners'}>
						<Parthners main lang={this.state.language}/>
					</ScrollableAnchor>	
					<ScrollableAnchor id={'footer'}>
						<Footer main lang={this.state.language}/>
					</ScrollableAnchor>
				</div>
				<MessengerCustomerChat
					pageId="315092155764419"
					appId="239139337031405>"
				/>
			</div>
			
		);
	}
}

export default Main;