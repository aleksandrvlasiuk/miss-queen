import React from 'react';
import Logo from '../../../logo_pink.png';
import './more-head.css';

class MoreHead extends React.Component {
	render() {
		return (
			<div id='more-head'>
				<div className="more-logo">
					<img src={Logo} alt="logo" />
					<p>{this.props.lang === "Eng" ? "Miss Queen Europe is a European competition aimed at promoting the true beauty, femininity and nobility." : "Miss Queen Europe - это всеевропейский конкурс, целью которого является продвижение истинной красоты, женственности и благородства."}</p>
					<p>{this.props.lang === "Eng" ? "Miss Queen Europe is a grandiose and unique project. Representatives from more than 50 European countries participate in the Miss Queen Europe competition. To date, qualifying rounds are held in more than 50 countries in Europe, more than 24000 applicants will take part in the castings." : "Miss Queen Europe - грандиозный и уникальный проект. В конкурсе Miss Queen Europe принимают участие представители более чем 50 европейских стран. На сегодняшний день квалификационные туры проводятся в более чем 50 странах Европы, в которых будут участвовать более 24000 номинанток."}</p>
					<p></p>
				</div>
				<div className="more-parts">
					<h2>{this.props.lang === "Eng" ? "Stages of the competition:" : "Этапы конкурса:"}</h2>
					<ul>
						<li>{this.props.lang === "Eng" ? "preliminary stage - general castings in 50 countries in Europe and the CIS" : "предварительный этап – общие кастинги в 50 странах Европы и СНГ"}</li>
						<li>{this.props.lang === "Eng" ? "selection stage for admission to voting and final casting in each country" : "этап отбора для допуска к голосованию и финальный кастинг в каждой из стран"}</li>
						<li>{this.props.lang === "Eng" ? "national online voting stage in each of the countries according to the road map of the competition" : "этап национального он-лайн голосования в каждой из стран согласно дорожной карте конкурса"}</li>
						<li>{this.props.lang === "Eng" ? "Grand Final - the grandest European social event to be held in the Ukraine, Kyiv" : "Grand final – грандиознейший европейский светский раут, который пройдет в Украине, Киев."}</li>
					</ul>
				</div>
			</div>
		);
	}
}

export default MoreHead;

