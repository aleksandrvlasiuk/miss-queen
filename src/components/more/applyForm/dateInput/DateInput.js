import React from 'react';
import DatePicker from 'react-date-picker';
import './date-input.css';

class DateInput extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			date: new Date()
		}
		this.changeDate = this.changeDate.bind(this);
	}
	changeDate(date) {
		this.setState({
			date
		})
	}
	render() {
		return (
			<div className="form-group form-birth">
				<label htmlFor={this.props.name} className='form-label'>{this.props.title}</label>
				<br/>
				<DatePicker
					onChange={this.changeDate}
					value={this.state.date}
					minDetail="decade"
					required
        		/>
			</div>
		);
	}
}

export default DateInput;

