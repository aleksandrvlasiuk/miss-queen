import React from 'react';
import Select from 'react-select';
import './select-country.css';

class SelectCountry extends React.Component {
	render() {
		const options = [
			{ value: 'Albania', label: 'Albania' },
			{ value: 'Armenia', label: 'Armenia' },
			{ value: 'Austria', label: 'Austria' },
			{ value: 'Belarus', label: 'Belarus' },
			{ value: 'Belgium', label: 'Belgium' },
			{ value: 'Bosnia', label: 'Bosnia' },
			{ value: 'Bulgaria', label: 'Bulgaria' },
			{ value: 'Croatia', label: 'Croatia' },
			{ value: 'Cyprus', label: 'Cyprus' },
			{ value: 'Czech', label: 'Czech' },
			{ value: 'Denmark', label: 'Denmark' },
			{ value: 'Estonia', label: 'Estonia' },
			{ value: 'Finland', label: 'Finland' },
			{ value: 'France', label: 'France' },
			{ value: 'Georgia', label: 'Georgia' },
			{ value: 'Germany', label: 'Germany' },
			{ value: 'Greece', label: 'Greece' },
			{ value: 'Hungary', label: 'Hungary' },
			{ value: 'Iceland', label: 'Iceland' },
			{ value: 'Ireland', label: 'Ireland' },
			{ value: 'Israel', label: 'Israel' },
			{ value: 'Italy', label: 'Italy' },
			{ value: 'Kazakhstan', label: 'Kazakhstan' },
			{ value: 'Latvia', label: 'Latvia' },
			{ value: 'Lithuania', label: 'Lithuania' },
			{ value: 'Macedonia', label: 'Macedonia' },
			{ value: 'Malta', label: 'Malta' },
			{ value: 'Moldova', label: 'Moldova' },
			{ value: 'Montenegro', label: 'Montenegro' },
			{ value: 'Netherlands', label: 'Netherlands' },
			{ value: 'Norway', label: 'Norway' },
			{ value: 'Poland', label: 'Poland' },
			{ value: 'Portugal', label: 'Portugal' },
			{ value: 'Romania', label: 'Romania' },
			{ value: 'Russia', label: 'Russia' },
			{ value: 'Serbia', label: 'Serbia' },
			{ value: 'Slovakia', label: 'Slovakia' },
			{ value: 'Spain', label: 'Spain' },
			{ value: 'Sweden', label: 'Sweden' },
			{ value: 'Switzerland', label: 'Switzerland' },
			{ value: 'Turkey', label: 'Turkey' },
			{ value: 'Ukraine', label: 'Ukraine' },
		];
		return (
			<div className="form-group">
				<label htmlFor={this.props.name} className='form-label'>{this.props.title}</label>
				<Select options={options} />
			</div>
		);
	}
}

export default SelectCountry;

