import React from 'react';
import './input.css';

class Input extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: ' '
		}
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(e) {
		this.props.handleInput(e.target);
		this.setState({
			value: e.target.value
		})
	}
	render() {
		return (
			<div className="form-group">
				<label htmlFor={this.props.name} className="form-label">{this.props.title}</label>
				<input
					className= {this.state.value.length < 1 ? 'form-control form-control-danger' : 'form-control'}
					id={this.props.name}
					name={this.props.name}
					type={this.props.type}
					value={this.props.value}
					placeholder={this.props.placeholder} 
					onChange = {this.handleChange}
					required
				/>
				{this.state.value.length < 1 ? <small style={{color: 'red'}}>* Must be required</small> : null}
			</div>
		);
	}
}

export default Input;

