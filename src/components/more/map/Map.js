import React from 'react';
import map from '../../../map_bg.png';
import logo from '../../../logo_black.png';
import './map.css';

const country = [
	{
		month: 'March',
		couts: ['Finland', 'Sweden', 'Liechtenstein', 'Norway', 'Ireland','Denmark'],
		flags: [ './flags/finland.png', './flags/sweden.png', './flags/liechtenstein.png', './flags/norway.png', './flags/ireland.png', './flags/iceland.png']
	},
	{
		month: 'April',
		couts: ['Netherlands', 'San-Marino', 'Luxembourg', 'Belgium', 'Germany', 'Poland', "Liberland", "Andorra"],
		flags: ['./flags/netherland.png', './flags/sanmarino.png', './flags/Luxembourg.png', './flags/belgium.png', './flags/germany.png', './flags/poland.png', './flags/liberland.png', './flags/andorra.png' ]
	},
	{
		month: 'May',
		couts: ['Switzerland', 'Austria', 'Hungary', 'Slovakia', 'Serbia', 'Macedonia', 'Ukraine'],
		flags: [ './flags/switzerland.png', './flags/austria.png', './flags/hundary.png', './flags/slovakia.png', './flags/servia.png', './flags/macedonia.png', './flags/ukraine.png' ]
	},
	{
		month: 'June',
		couts: ['Lithuania', 'Latvia', 'Estonia', 'Romania', 'Bulgaria', 'Bosnia', 'Czech'],
		flags: [ './flags/lithuania.png', './flags/latvia.png', './flags/estonia.png', './flags/romania.png', './flags/bulgaria.png', './flags/bosnia.png', './flags/czech.png' ]
	},
	{
		month: 'July',
		couts: ['Cyprus', 'Monako', 'Greece', 'Albania', 'Montenegro', 'Croatia', 'Turkey', 'Tunisia'],
		flags: [ './flags/cyprus.png', './flags/monako.png', './flags/greece.png', './flags/albania.png', './flags/montenegro.png', './flags/croatia.png', './flags/turkey.png', './flags/tunisia.png' ]
	},
	{
		month: 'August',
		couts: ['Malta', 'Italy', 'France', 'Marocco', 'Spain', 'Portugal', 'Azerbaijan'],
		flags: [ './flags/malta.png', './flags/italy.png', './flags/france.png','./flags/marocco.png', './flags/spain.png', './flags/portugal.png', './flags/azerbaijan.png' ]
	},
	{
		month: 'September',
		couts: [ 'Belarus', 'Moldova', 'Georgia', 'Kazakhstan', 'Russia', 'Israel', 'Armenia'],
		flags: [ './flags/belarus.png', './flags/moldova.png','./flags/georgia.png', './flags/kazakhstan.png', './flags/russia.png', './flags/israel.png', './flags/armenia.png' ]
	}
];

class Map extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			lang: this.props.lang || "Eng"
		}
	}
	render() {
		return (
			<div className="map">
				<div className="map-container">
					<img src={map} alt="map" />	
					<div className="map-country">
						{country.map(function(item, i) {
							return (<div key={i}>
								<img src={logo} alt="logo" />
								<h3>{item.month}</h3>
								<div>
									<ul>
										{item.couts.map(function(count, id) {
											let url = require('./flags/' + count.toLowerCase() + '.png');
											return (<li key={id}>
														<div className="flag">
															<img src={url} alt={count} className="flag-img"/>
														</div>
														{count}
													</li>);
										})}
									</ul>
								</div>
							</div>)
						})}
					</div>
				</div>
			</div>
		);
	}
}

export default Map;