import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../1.gif';
import './logo.css';

class Logo extends React.Component {
	render() {
		return (
			<div className="logo">
				<Link to='/#page-wrap'>
					<img src={logo} alt="logo"/>
				</Link>
			</div>
		);
	}
}

export default Logo;