import React, { Component } from 'react';
import './packagetitle.css'

class PackageTiele extends Component {
	render() {
		return (
			<ul className="package-title">
				<li>Job Offers</li>
				<li>Build your Career</li>
				<li>Attend Exclusive Events</li>
				<li>Experience Luxury Travel</li>
				<li>Grow your Network</li>
				<li>Tailored Personal Introductions</li>
				<li>Create and share projects</li>
			</ul>
		);
  	}
}

export default PackageTiele;
