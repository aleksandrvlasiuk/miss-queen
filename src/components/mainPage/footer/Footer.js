import React, { Component } from 'react';
import Social from './social/Social';
import FooterNav from './footer-nav/FooterNav';
import Contacts from './contacts/Contacts';
import logo from './white_logo.gif'
import './footer.css'

class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<div className='main-footer'>
					<FooterNav main={this.props.main} lang={this.props.lang}/>
					<Contacts />
				</div>
				<div className="footer-border"></div>
				<div className="semi-footer">
					<Social />
					<strong className="logo-footer">
						<img src={logo} alt="logo footer"/>
					</strong>
					<div className="footer-copyright">
						<em className="copy">2018 &copy; Miss Queen Europe. All rights reserved.</em>
					</div>
				</div>
			</div>
		);
  	}
}

export default Footer;
