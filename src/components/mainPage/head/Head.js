import React from 'react';
import HeadVideo from './HeadVideo/HeadVideo';
import HeadContainer from './HeadContainer/HeadContainer';
import { Link } from 'react-router-dom';
import ruleseng from './ruleseng.pdf';
import rulesrus from './rulesrus.pdf';

import './head.css';

class Head extends React.Component {
	constructor(props) {
		super(props);
		this.changeLang = this.changeLang.bind(this);
		this.state = {
			lang: "Eng"
		}
	}
	changeLang(e) {
		e.preventDefault();
		localStorage.setItem('language', e.target.innerText);
		this.props.updateData(e.target.innerText);
		this.setState({
			lang: e.target.innerText
		})
	}
	render() {
		return (
			<div className="head">
				<HeadVideo />
				<HeadContainer lang={this.props.lang}/>
				<div className="head-lang">
					<a href="/" className={localStorage.getItem('language') === "Eng" || !localStorage.getItem('language') ? "active-lang" : "" } onClick={this.changeLang}>Eng</a>
					<a href="/" className={localStorage.getItem('language') === "Rus" ? "active-lang" : "" } onClick={this.changeLang}>Rus</a>
				</div>
				<div className="terms">
					<Link to={{pathname: '/queens', lang: this.props.lang}}>{this.props.lang === "Eng" ? "Winners" : "Победительницы"}</Link>
					<a href={localStorage.getItem('language') === "Rus" ? rulesrus : ruleseng }>{localStorage.getItem('language') === "Rus" ? "Правила и условия" : "Rules and terms" }</a>
				</div>
				<div className="buy-tickets-link">
					<a href="https://wa.me/+380638132296">{this.props.lang === "Eng" ? "Buy ticktes" : "Купить билет"}</a>
				</div>
			</div>
		);
	}
}

export default Head;