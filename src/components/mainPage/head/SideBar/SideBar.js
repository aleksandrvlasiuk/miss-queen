import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import ButtonApply from '../HeadContainer/button-apply/ButtonApply';
import { Link } from 'react-router-dom';

import './sidebar.css';

class Head extends React.Component {
	constructor() {
		super();
		this.state = {
			opened: false
		}
		this.clickLink = this.clickLink.bind(this);
	}
	clickLink() {
		this.setState({
			opened: !this.state.opened
		})
	}
	render() {
		let screenWidth = window.screen.width;
		return (
			<Menu pageWrapId={ "page-wrap" } outerContainerId={ "outer-container" } 
				right
				isOpen = {this.state.opened}
				width={screenWidth < 768 ? '100%' : screenWidth > 1024 ? '30%' : '50%'}>
				{ 
					this.props.main ? 
					<a href='#home'>{this.props.lang === "Eng" ? "Home" : "Главная"}</a> :
					<Link to={'/#home'}>{this.props.lang === "Eng" ? "Home" : "Главная"}</Link>
				}
				{ 
					this.props.main ? 
					<a href='#about'>{this.props.lang === "Eng" ? "About" : "О нас"}</a> :
					<Link to={'/#about'}>{this.props.lang === "Eng" ? "About" : "О нас"}</Link>
				}
				{ 
					this.props.main ? 
					<a href='#goals'>{this.props.lang === "Eng" ? "Goals" : "Задачи"}</a> :
					<Link to={'/#goals'}>{this.props.lang === "Eng" ? "Goals" : "Задачи"}</Link>
				}
				{ 
					this.props.main ? 
					<a href='#services'>{this.props.lang === "Eng" ? "Services" : "Сервисы"}</a> :
					<Link to={'/#services'}>{this.props.lang === "Eng" ? "Services" : "Сервисы"}</Link>
				}
				{ 
					this.props.main ? 
					<a href='#parthners'>{this.props.lang === "Eng" ? "Partners" : "Партнеры"}</a> :
					<Link to={'/#parthners'}>{this.props.lang === "Eng" ? "Partners" : "Партнеры"}</Link>
				}
				{ 
					this.props.main ? 
					<a href='#footer'>{this.props.lang === "Eng" ? "Contacts" : "Контакты"}</a> :
					<Link to={'/#footer'}>{this.props.lang === "Eng" ? "Contacts" : "Контакты"}</Link>
				}
				{ 
					<Link to={{pathname: '/more', lang: this.props.lang}}><ButtonApply color={"rgb(75, 206, 75)"} text={this.props.lang === "Eng" ? "Apply" : "Apply"}/></Link>
				}
			</Menu>
		);
	}
}

export default Head;