import React from 'react';
import ButtonApply from './button-apply/ButtonApply';
import { Link } from 'react-router-dom';
import Modal from "../HeadContainer/reg-modal/Modal"

import logo from './../../../../1.gif'

import './headcontainer.css';

class HeadContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false
		}
		this.showModal = this.showModal.bind(this);
		this.updateData = this.updateData.bind(this);
	}

	showModal() {
		this.setState({
			isOpen: true
		})
	}
	updateData(value) {
		this.setState({ 
			isOpen: value 
		})
	 }

	render() {
		return (
				<div className="head-container">
					{this.state.isOpen ? <Modal isOpen={this.state.isOpen} updateData={this.updateData}/> : null}
					<div className="head-title">
						<h1>Miss Queen Europe</h1>
						<h3>{this.props.lang === "Eng" ? <p>Miss Queen Europe is a European competition aimed at promoting the true beauty, femininity and nobility.</p> : <p>Miss Queen Europe - это европейский конкурс, целью которого является продвижение истинной красоты, женственности и благородства</p>}</h3>
						<h2 className="final">Grand Final 2019 | Kyiv</h2>
						<div className="links-container">
							<button id="registration" className="registration" onClick={this.showModal} >{this.props.lang === "Eng" ? "Registration" : "Регистрация"}</button>
							<Link to={{pathname: '/more', lang: this.props.lang}}><ButtonApply text={this.props.lang === "Eng" ? "More" : "Подробнее"}/></Link>
						</div>
						<h4 className="children-restrict">{this.props.lang === "Eng" ? "*The contest does not restrict the participation of nominees that have children." : "*Конкурс не ограничивает участия номинанток с наличием детей." }</h4>
					</div>
					<div className="head-logo">
						<img src={logo} alt=""/>
					</div>
					<em className="head-copy">2018 &copy; Miss Queen Europe. All rights reserved.</em>
				</div>
		);
	}
}

export default HeadContainer;