import React from 'react';
import ApplyForm from "../../../../more/applyForm/ApplyForm";

import './modal.css';

export default class Modal extends React.Component {
	render() {
		return (
			<div className={this.props.isOpen ? "modal-reg open" : "modal-reg"}>
				<button className="close-modal" onClick={() => { this.props.updateData(!this.props.isOpen)}}>X</button>
				<ApplyForm lang={localStorage.getItem("language")}/>
			</div>
		);
	}
}