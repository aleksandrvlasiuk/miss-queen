import React from 'react';
import video from './main.mp4'
import './headvideo.css';

class HeadVideo extends React.Component {
	render() {
		return (
			<video className="background-video" loop autoPlay muted >
				<source src={video} type="video/mp4" />
				Your browser does not support the video tag.
			</video>
		);
	}
}

export default HeadVideo;