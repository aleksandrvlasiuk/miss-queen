import React, {Fragment} from 'react';
import NewButtonApply from './button-apply/NewButtonApply';
import { Link } from 'react-router-dom';
import NewModal from "../NewHeadContainer/reg-modal/NewModal"

import logo from '../../../../logo_white.png'

import './headcontainer.css';

class NewHeadContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false
		}
		this.showModal = this.showModal.bind(this);
		this.updateData = this.updateData.bind(this);
	}

	showModal() {
		this.setState({
			isOpen: true
		})
	}
	updateData(value) {
		this.setState({ 
			isOpen: value 
		})
	 }

	render() {
		const flags = ['Finland', 'Sweden', 'Liechtenstein', 'Norway', 'Ireland','Denmark','Netherlands', 'San-Marino', 'Luxembourg', 'Belgium', 'Germany', 'Poland', "Liberland", "Andorra",
		'Switzerland', 'Austria', 'Hungary', 'Slovakia', 'Serbia', 'Macedonia', 'Ukraine','Lithuania', 'Latvia', 'Estonia', 'Romania', 'Bulgaria', 'Bosnia', 'Czech',
		'Cyprus', 'Monako', 'Greece', 'Albania', 'Montenegro', 'Croatia', 'Turkey', 'Tunisia','Malta', 'Italy', 'France', 'Marocco', 'Spain', 'Portugal', 'Azerbaijan',
		'Belarus', 'Moldova', 'Georgia', 'Kazakhstan', 'Russia', 'Israel', 'Armenia'];
		const artists = [
			{
				name: "Yann Destal (Modjo)",
				img: "modjo"
			},
			{
				name: "Sophie Ellis-Bextor",
				img: "ellie"
			},
			{
				name: "Haddaway",
				img: "haddaway"
			},
			{
				name: "Bosson",
				img: "bosson"
			}
		];
		return (
			<Fragment>
				<div className="head-container">
					{this.state.isOpen ? <NewModal isOpen={this.state.isOpen} updateData={this.updateData}/> : null}
					<div className="primary-header">
						<p>{this.props.lang === "Eng" ? "KYIV" : "КИЕВ"} <br /> 02.11.2019</p>
						<img src={logo} alt="big logo" />
						<p>{this.props.lang === "Eng" ? "INTERNATIONAL CONVENTIONAL CENTER PARKOVY" : "МЕЖДУНАРОДНЫЙ КОНФЕРЕНЦ-ЦЕНТР PARKOVY"}</p>
					</div>
					<div className="grand-final-title">
						<h1>{this.props.lang === "Eng" ? "Grand Final" : "Гранд Финал"}</h1>
					</div>
					<div className="grand-countries">
						<div>
							{flags.map((item, id) => {
								if(id < 25) {
									let url = require('../../../more/map/flags/' + item.toLowerCase() + '.png');
									return <img src={url} alt="flag" key={id + 120} />;
								}
								else {
									return false;
								}
							})}
						</div>
							<p>50<br/>{this.props.lang === "Eng" ? "countries" : "стран"}</p>
						<div>
							{flags.map((item, id) => {
								if(id > 24) {
									let url = require('../../../more/map/flags/' + item.toLowerCase() + '.png');
									return <img src={url} alt="flag" key={id + 220} />;
								}
								else {
									return false;
								}
							})}
						</div>
					</div>
					<div className="artists">
						{artists.map((item,id) => {
							let url = require("./" + item.img.toLowerCase() + '.png');
							return (
								<div className="artists-item" key={300+id}>
									<img src={url} alt="artist"/>
									<p>{item.name}</p>
								</div>
							);
						})}
					</div>
					
					<div className="head-logo">
						<img src={logo} alt=""/>
					</div>
					<em className="head-copy">2018 &copy; Miss Queen Europe. All rights reserved.</em>
				</div>
				<div className="head-title"> 
					<div className="links-container">
						<button id="registration" className="registration" onClick={this.showModal} >{this.props.lang === "Eng" ? "Registration" : "Регистрация"}</button>
						<Link to={{pathname: '/more', lang: this.props.lang}}><NewButtonApply text={this.props.lang === "Eng" ? "More" : "Подробнее"}/></Link>
					</div>
					<h4 className="children-restrict">{this.props.lang === "Eng" ? "*The contest does not restrict the participation of nominees that have children." : "*Конкурс не ограничивает участия номинанток с наличием детей." }</h4>
				</div>
				</Fragment>
		);
	}
}

export default NewHeadContainer;