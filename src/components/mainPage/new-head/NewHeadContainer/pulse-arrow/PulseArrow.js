import React from 'react';
import './pulse-arrow.css';

class PulseArrow extends React.Component {
	render() {
		return (
			<div className='pulse-arrow'>
				<a href={this.props.to}>
					<svg xmlns="http://www.w3.org/2000/svg">
						<use href="#icon-arrow_downward">
							<svg viewBox="0 0 24 24" id="icon-arrow_downward" width="100%" height="100%">
								<title>arrow_downward</title>
								<path d="M20.016 12L12 20.016 3.984 12l1.453-1.406 5.578 5.578V3.984h1.97v12.188l5.624-5.578z"></path>
							</svg>
						</use>
					</svg>
				</a>
			</div>
		);
  	}
}

export default PulseArrow;